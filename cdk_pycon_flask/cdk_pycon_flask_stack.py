from aws_cdk import aws_ec2, aws_ecs, aws_ecs_patterns, Stack, CfnOutput
from constructs import Construct

class CdkPyconFlaskStack(Stack):

    def __init__(self, scope: Construct, construct_id: str, **kwargs) -> None:
        super().__init__(scope, construct_id, **kwargs)

        vpc = aws_ec2.Vpc(self, 'FargateFlaskVPC', cidr='10.0.0.0/16')

        # ECS cluster
        cluster = aws_ecs.Cluster(self, 'Cluster', vpc=vpc)
        svc = aws_ecs_patterns.ApplicationLoadBalancedFargateService(
            self, 'FargateService',
            cluster=cluster,
            task_image_options={
                'image':aws_ecs.ContainerImage.from_asset('flask-docker-app'),
                'container_port':5000,
                'environment':{
                    'PLATFORM': 'AWS Fargate :-)'
                }
            },
        )

        CfnOutput(self, 'SericeURL', value="http://{}".format(
            svc.load_balancer.load_balancer_dns_name))
